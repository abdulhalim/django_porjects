from django.contrib import admin

from .models import Post


class PostModelAdmin(admin.ModelAdmin):
	class Meta:
		model = Post
	
	list_display = ["title", "created", "updated"]
	list_display_links = ["title"]
	list_filter = ['updated', 'created']
	search_fields = ['title', 'content']

admin.site.register(Post, PostModelAdmin)