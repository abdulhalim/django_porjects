from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse
from django.db.models import Q
from django.contrib import messages
from django.utils import timezone

from urllib.parse import quote

# Modules for pagination
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Pagination module ends

from .forms import PostForm

from .models import Post


def post_list(request):
	today = timezone.now().date()
	# queryset_list = Post.objects.filter(draft=False).filter(publish__lte=timezone.now()) #all().order_by('-created')
	queryset_list = Post.objects.active()
	if request.user.is_staff or request.user.is_superuser:
		queryset_list = Post.objects.all()

	query = request.GET.get('q')
	if query:
		queryset_list = queryset_list.filter(
			Q(title__icontains=query) |
			Q(content__icontains=query) |
			Q(user__first_name__icontains=query) |
			Q(user__last_name__icontains=query)
		).distinct()

	paginator = Paginator(queryset_list, 5) # Show 20 contacts per page
	page = request.GET.get('page')
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)
	context = {
		"title": "Posts List",
		"queryset": queryset,
		"today": today,
	}
	return render (request, "post_list.html", context)

def post_detail(request, slug=None):
	instance = get_object_or_404(Post, slug=slug)
	if instance.draft or instance.publish > timezone.now().date():
		if not request.user.is_staff or not request.user.is_superuser:
			raise Http404
	share_string = quote(instance.content)
	context = {
		"instance": instance,
		"title": instance.title,
		"share_string": share_string
	}
	return render(request, "detail.html", context)




def create_post(request):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404

	# if not request.user.is_authenticated():
	# 	raise Http404

	form = PostForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.user = request.user
		instance.save()
		messages.success(request, "Successfully created!")
		return redirect(reverse('posts:index'))
	context = {
		"title":"Create Post",
		"form": form,
	}
	return render (request, "post_form.html", context)


def update_post(request, slug=None):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	instance = get_object_or_404(Post, slug=slug)
	form = PostForm(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "Successfully updated!")
		return redirect('posts:detail', slug=slug)

	context = {
		"instance": instance,
		"title": "Update",
		"form": form
	}
	return render(request, "post_form.html", context)


def delete_post(request, id=None):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	instance = get_object_or_404(Post, id=id)
	instance.delete()
	messages.success(request, "Successfully deleted")
	return redirect('posts:index')


