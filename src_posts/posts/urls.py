
from django.conf.urls import url
from django.contrib import admin

# from . import views
from .views import (
	post_list,
	create_post,
	post_detail,
	update_post,
	delete_post,
)

urlpatterns = [

    # Direct import the link
    # url(r'^posts/$', '<app_name>.views.<function_name>')
    url(r'^$', post_list, name='index'),   
 	url(r'^create/$', create_post, name='create'),
 	url(r'^(?P<slug>[-\w]+)/edit/$', update_post, name='update'),
 	url(r'^(?P<id>\d+)/delete/$', delete_post, name='delete'),
 	url(r'^(?P<slug>[-\w]+)/$', post_detail, name='detail'),
 	
]
