from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render
from .forms import ContactForm
from django.core.mail import send_mail
import datetime


def hello(request):
    #host = request.is_secure()
    # host = request.get_full_path()
    # host = request.get_host()
    # host = request.path

    # try:
    #     ua = request.META['HTTP_USER_AGENT']
    # except KeyError:
    #     ua = 'Unknown'

    # withou try: except catch
    ua = request.META.get('HTTP_USER_AGENT', 'Unknown')

    return HttpResponse("Welcome to the page at %s <br/> and Your user agent is %s" % (request.path, ua))


def display_meta(request):
    values = request.META.items()
    values=sorted(values)
    html = []
    for k, v in values:
        html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, v))
    return HttpResponse('<table>%s</table>' % '\n'.join(html))


def current_datetime(request):
    now = datetime.datetime.now()
    return render(request,
                'current_datetime.html',
                {
                    'title': 'Current datetime',
                    'current_date':now.date(),
                    'current_time': now.time(),
                })


def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    #assert False
    html = "In %s hour(s), it will be %s." % (offset, dt)
    return HttpResponse(html)


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            send_mail(
                cd['subject'],
                cd['message'],
                cd.get('email', 'noreplay@example.com'),
                ['abdulhalim.cu10@gmail.com'],
            )
            return HttpResponseRedirect('/contact/')
    else:
        form = ContactForm()
    return render(request, 'contact_form.html', {'form':form})



def debug(request):
    return HttpResponse("DEBUG is True")