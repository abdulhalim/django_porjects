from django.shortcuts import render
from django.http import HttpResponse

from .models import Book, Publisher
from django.views.generic import ListView, DetailView


class PublisherDetail(DetailView):
    model = Publisher
    # context_object_name = 'publisher'
    # queryset = Publisher.objects.all()

    def get_context_data(self, **kwargs):
        # call the base implementation first to get a context
        context = super(PublisherDetail, self).get_context_data(**kwargs)
        # add in a queryset of all the books
        context['book_list'] = Book.objects.all()
        return context


class BookList(ListView):
    queryset = Book.objects.order_by('-publication_date')
    context_object_name = 'book_list'


class PublisherList(ListView):
    model = Publisher
    context_object_name = 'my_favorite_publishers'


def homepage(request):
    return render(request, 'homepage.html')


def search_form(request):
    return render(request, 'search_form.html')


def search(request):
    # if 'q' in request.GET
    # other two methods are BAD
    # or if request.GET.get("q")
    # or if request.GET['q']
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            errors.append('Enter a search term.')
        elif len(q) > 20:
            errors.append('Please enter at most 20 characters.')
        else:
            books = Book.objects.filter(title__icontains=q)
            return render(request, 'search_results.html', {'books':books, 'query':q})

    return render(request, 'search_form.html', {'errors':errors})


def year_archive(request, year=2003):
    year = year
    context = {
        "year":year
    }
    return render(request, 'year_archive.html', context)


