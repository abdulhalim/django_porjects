from django import template
from django.template.defaultfilters import stringfilter
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
import datetime

register = template.Library()

"""

def cut(value, arg):
    # Removes all values of arg from the given string.
    return value.replace(arg, '')


def lower(value):
    # Converts a string into all lowercase
    return value.lower()


register.filter('cut', cut)
register.filter('lower', lower)


"""


@register.filter(name='cut')
def cut(value, arg):
    """Removes all values of arg from the given string."""
    return value.replace(arg, '')


@register.filter
@stringfilter
def lower(value):
    """Converts a string into all lowercase"""
    return value.lower()


@register.filter(needs_autoescape=True)
def initial_letter_filter(text, autoescape=None):
    first, other = text[0], text[1:]
    if autoescape:
        esc = conditional_escape
    else:
        esc = lambda x: x
    result = '<strong>%s</strong>%s' % (esc(first), esc(other))
    return mark_safe(result)


@register.filter(expects_localtime=True)
def business_hours(value):
    try:
        return 9 <= value.hour < 17
    except AttributeError:
        return ''


@register.simple_tag
def current_time(format_string):
    return datetime.datetime.now().strftime(format_string)


from books.models import Book


def books_for_author(author):
    books = Book.objects.filter(authors__id=author.id)
    return {'books': books}