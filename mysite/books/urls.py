from django.conf.urls import url, include

from . import views


urlpatterns = [
    url(r'^$', views.homepage),
    url(r'^search-form/$', views.search_form),
    url(r'^search/$', views.search),
    # url(r'^reviews/2003/$', views.special_case_2003),
    url(r'^(?P<year>[0-9]{4})/$', views.year_archive, name='reviews-year-archive'),
    # url(r'^(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.month_archive),
    # url(r'^(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]]{2})/$', views.review_detail),
    url(r'^publishers/$', views.PublisherList.as_view()),
]